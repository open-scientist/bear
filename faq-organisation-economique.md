
## Quel projet peut être portée par la coopérative

Voici l'un des premiers projets envisagé :

Les agricultrices et agriculteurs réalisent des expériences d'association de plantes, de rotation de cultures et de réduction de travail des sols. Toutefois, ces données sont peu prises en compte.

La proposition est de coordonner ces expériences et permettre aux personnes les réalisant d'être rémunérées dans le cadre d'un contrat de travail en CAE pour ce travail.


