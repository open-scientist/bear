# BEAR – Be a researcher

## Organisation économique

[En lire plus](organisation-economique.md)

## Plateforme

[En lire plus](platform.md)

## Comment contribuer

Please tell us the features that you would like by [opening an issue](https://gitlab.com/open-scientist/bear/issues/new)
