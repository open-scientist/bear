# Coopérative d'expérimentation scientifique

Le double but de cette organisation est de permettre à des individus d'exercer une activité scientifique rémunérée, ainsi que d'assurer la production de données scientifiques nécessaires pour s'atteler aux questions de société ouvertes.

La [charte de la coopérative](charte.md) base l'exigence de l'organisation horizontale ainsi que la publication uniquement de productions scientifiques reproductibles.

## Présentation

La coopérative a pour but de rassembler des personnes réalisant des mesures et expériences scientifiques à temps plein ou en complément d'un autre emploi.

La coopérative produit des données et résultats scientifiques respectant les critères de science ouverte et reproductible. Les travaux réalisés par la coopérative contribuent à produire des jeux de données exhaustifs (en particuliers sur la distribution géographique de mesures).

En interne, la gouvernance est partagée.

## Réalisation d'expérimentation scientifique

Les projets incluant mesures et expérimentations scientifiques réalisés au sein de la coopérative sont des projets apportant des données pour répondre à des problématiques de société.

La coopérative vise à mettre en place la mutualisation de matériels et blueprints d'auto-fabrication de montages expérimentaur et d'instruments de mesure low-tech, tant que la qualité des mesures est assurée.

Les données produites sont diffusées sous licence libre et avec les métadonnées assurant la reproductibilité du processus les produisant.

## Inscription dans l'économie

La participation aux travaux de la coopérative est ouverte. La coopérative lutte contre le travail gratuit et rémunère les personnes contribuant aux projets.

La recherche de clients et financements est réalisée collectivement et amenant à une distribution du travail au sein du groupe.

## Comment contribuer

Ce projet de coopérative est à l'étude et sera ce que les personnes qui souhaitent y contribuer en feront.

Vos retours, questions et suggestions seront grandement appréciées. Pour contribuer, voici un [guide de contribution](CONTRIBUTING-organisation-economique.md) :)

## Foire aux questions (FAQ)

[consulter la FAQ](faq-organisation-economique.md)

